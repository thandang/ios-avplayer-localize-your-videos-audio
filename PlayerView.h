//
//  AVPlayerView.h
//  Lorrha
//
//  Created by Kenny Sanchez on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PlayerView : UIView

@property (nonatomic, strong) AVPlayer *player;

- (void)setVideoFillMode:(NSString *)fillMode;


@end
